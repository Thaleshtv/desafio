package teste;

import static org.junit.Assert.assertEquals;



import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;


public class testeee {
	
	
	@Test
	public void testandoFluxo() {
		
		WebDriverManager.chromedriver().setup(); 
		WebDriver driver = new ChromeDriver();
		driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
		
		WebElement userNameBox = driver.findElement(By.name("username")); 
		WebElement passWordBox = driver.findElement(By.name("password")); 
		WebElement commentsBox = driver.findElement(By.name("comments"));
		
		WebElement checkBox1 = driver.findElement(By.xpath("//input[ @value= 'cb1']")); 
		WebElement checkBox2 = driver.findElement(By.xpath("//input[ @value= 'cb2']")); 

		WebElement radio1 = driver.findElement(By.xpath("//input[ @value='rd1']")); 
		
		WebElement selection1 = driver.findElement(By.xpath("//option [@value = 'ms1']")); 
		WebElement selection4 = driver.findElement(By.xpath("//option [@value = 'ms4']")); 
		//DD4 j� vinha selecionado no formulario,ent�o para tirar,cliquei novamente
	
		
		WebElement dropdown = driver.findElement(By.xpath("//option[ @value = 'dd1']"));
		WebElement submitButton = driver.findElement(By.xpath("//input[ @value = 'submit']"));
	
		userNameBox.sendKeys("QA User"); 
		passWordBox.sendKeys("QA Password"); 
		
		commentsBox.clear();
		commentsBox.sendKeys("Comentario"); 
		
		checkBox1.click(); 
		checkBox2.click(); 

		
		radio1.click(); 
		selection1.click(); 	
		selection4.click(); 
		dropdown.click(); 
		submitButton.click(); 
		
		
		//Teste	
		Assert.assertEquals("QA User",driver.findElement(By.xpath("//*[@id=\"_valueusername\"]")).getText());
		Assert.assertEquals("QA Password",driver.findElement(By.xpath("//*[@id=\"_valuepassword\"]")).getText());
		Assert.assertEquals("Comentario",driver.findElement(By.xpath("//*[@id=\"_valuecomments\"]")).getText());	
		Assert.assertEquals("cb1",driver.findElement(By.xpath("//*[@id=\"_valuecheckboxes0\"]")).getText());
		Assert.assertEquals("cb2",driver.findElement(By.xpath("//*[@id=\"_valuecheckboxes1\"]")).getText());
		Assert.assertEquals("cb3",driver.findElement(By.xpath("//*[@id=\"_valuecheckboxes2\"]")).getText());
		Assert.assertEquals("rd1",driver.findElement(By.xpath("//*[@id=\"_valueradioval\"]")).getText());
		Assert.assertEquals("ms1",driver.findElement(By.xpath("//*[@id=\"_valuemultipleselect0\"]")).getText());
		Assert.assertEquals("dd1",driver.findElement(By.xpath("//*[@id=\"_valuedropdown\"]")).getText());
		
	}
	
	
		

		

}
	


