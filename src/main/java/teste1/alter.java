package teste1;


import static org.junit.Assert.assertEquals;


import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class alter {
	
	
	@Test
	public void testando() {
		WebDriverManager.chromedriver().setup(); 
		WebDriver driver = new ChromeDriver();
		driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
		
		
		WebElement commentsBox = driver.findElement(By.name("comments"));
			
		WebElement submitButton = driver.findElement(By.xpath("//input[ @value = 'submit']"));
		

		
		commentsBox.clear();
		submitButton.click();
		
		Assert.assertEquals("No Value for username",driver.findElement(By.xpath("/html/body/div/div[3]/p[1]/strong")).getText());
		Assert.assertEquals("No Value for password",driver.findElement(By.xpath("/html/body/div/div[3]/p[2]/strong")).getText());
		Assert.assertEquals("No Value for comments",driver.findElement(By.xpath("/html/body/div/div[3]/p[3]/strong")).getText());
	}

}
